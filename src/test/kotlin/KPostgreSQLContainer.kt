import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.utility.DockerImageName

class KPostgreSQLContainer(dockerImageName: DockerImageName) : PostgreSQLContainer<KPostgreSQLContainer>(dockerImageName)
