import com.assers.ass.database.*
import com.assers.ass.models.Commands
import com.assers.ass.models.SeriesStatus
import com.assers.ass.models.mock.Message
import org.junit.Rule
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName

@Testcontainers
class TestBotCommands {

    @Test
    fun `series was added`() {
        var res = seriesTable[1, "Test"]
        assertNull(res)
        Commands.ADD.action(Message("/add Test", 1))
        res = seriesTable[1, "Test"]
        println(res)
        assertNotNull(res)
    }

    @Test
    fun `status was updated automatically after incrementing last series`() {
        var status = seriesTable[1, "Arcane"]!!.status
        assertTrue(status == SeriesStatus.WATCHING)
        Commands.INC.action(Message("/inc Arcane", 1))
        status = seriesTable[1, "Arcane"]!!.status
        println(status.name.lowercase())
        assertTrue(status == SeriesStatus.FINISHED)
    }

    @Test
    fun `series was removed`() {
        seriesTable.add(1, "Remove")
        var series = seriesTable[1, "Remove"]
        assertNotNull(series)
        Commands.REMOVE.action(Message("/remove Remove", 1))
        series = seriesTable[1, "Remove"]
        assertNull(series)
    }

    @Test
    fun `series last episode counter was updated`() {
        var series = seriesTable[1, "Naruto"]
        assertTrue(series!!.currentEpisode==127 && series.status == SeriesStatus.DROPPED)
        Commands.UPDATE.action(Message("/update Naruto --status watching --current 128", 1))
        series = seriesTable[1, "Naruto"]
        assertTrue(series!!.currentEpisode==128 && series.status == SeriesStatus.WATCHING)
    }

    @Test
    fun `series result was shown`() {
        val res = Commands.SHOW.action(Message("/show Бригада", 1))
        assertEquals(
            """
                Бригада statistics:
                status: FINISHED
                current episode: 15
                total episode: 15
                progress: 100%
            """.trimIndent(),
            res
        )
    }

    companion object {
        @Rule
        @Container
        val postgres = KPostgreSQLContainer(DockerImageName.parse("postgres:14-alpine"))

        @JvmStatic
        @BeforeAll
        fun prepareDB() {
            prepareDatabase(DatabaseInfo(postgres.jdbcUrl, postgres.username, postgres.password))
            seriesTable.add(1, "Arcane", currentEpisode = 8, totalEpisodes = 9)
            seriesTable.add(1, "Naruto", SeriesStatus.DROPPED, currentEpisode = 127, totalEpisodes = 256)
            seriesTable.add(1, "Бригада", currentEpisode = 15, totalEpisodes = 15)
        }
    }

}
