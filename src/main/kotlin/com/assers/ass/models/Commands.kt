package com.assers.ass.models

import com.assers.ass.database.add
import com.assers.ass.database.get
import com.assers.ass.database.seriesTable
import com.assers.ass.database.deriveSeriesStatus
import com.assers.ass.models.mock.Message
import com.assers.ass.utils.*
import org.ktorm.dsl.eq
import org.ktorm.entity.count
import org.ktorm.entity.joinToString
import org.ktorm.entity.sortedBy
import org.postgresql.util.PSQLException

enum class Commands(val text: String, val action: (Message) -> String) {
    ADD("/add", {
        try {
            val requestText = it.text
            if (requestText.trim() == "/add") {
                addHelpText()
            } else {
                val nameAndArguments = parseNameAndArguments(requestText.removePrefix("/add"))
                seriesTable.add(
                    it.chatId,
                    nameAndArguments.seriesName,
                    SeriesStatus.values().find { status ->
                        nameAndArguments.arguments["status"]?.lowercase().equals(status.name.lowercase())
                    },
                    nameAndArguments.arguments["current"]?.toInt() ?: 0,
                    nameAndArguments.arguments["total"]?.toInt()
                )
                "New series was added."
            }
        } catch (e: PSQLException) {
            e.printStackTrace()
            "Error. You already have this series in your collection."
        } catch (e: Exception) {
            e.printStackTrace()
            "Error. Please recheck the correctness of provided data."
        }
    }),
    UPDATE("/update", {
        try {
            val requestText = it.text
            if (requestText.trim() == "/update") {
                updateHelpText()
            } else {
                val nameAndArguments = parseNameAndArguments(requestText.removePrefix("/update"))
                val series = seriesTable[it.chatId, nameAndArguments.seriesName]
                if (series == null) {
                    "There is no series called \"${nameAndArguments.seriesName}\" in your list."
                } else {
                    val newCurrent = nameAndArguments.arguments["current"]
                    val newTotal = nameAndArguments.arguments["total"]
                    val newStatus = nameAndArguments.arguments["status"]


                    if (newCurrent != null) series.currentEpisode = newCurrent.toInt()
                    if (newTotal != null) series.totalEpisodes = newTotal.toIntOrNull()
                    series.status = if (newStatus != null) {
                        SeriesStatus.values().find { status -> status.name == newStatus.uppercase() }
                            ?: throw IllegalArgumentException("Incorrect status provided")
                    } else {
                        deriveSeriesStatus(series.currentEpisode, series.totalEpisodes)
                    }
                    series.flushChanges()

                    "Series \"${series.name}\" was successfully updated."
                }
            }
        } catch (e: PSQLException) {
            e.printStackTrace()
            "Error. An unknown database error has occurred."
        } catch (e: Exception) {
            e.printStackTrace()
            "Error. Please recheck the correctness of provided data."
        }
    }),
    REMOVE("/remove", {
        try {
            val requestText = it.text
            if (requestText.trim() == "/remove") {
                removeHelpText()
            } else {
                val nameAndArguments = parseNameAndArguments(requestText.removePrefix("/remove"))
                val series = seriesTable[it.chatId, nameAndArguments.seriesName]
                if (series == null) {
                    "There is no series called \"${nameAndArguments.seriesName}\" in your list."
                } else {
                    series.delete()
                    "Series \"${nameAndArguments.seriesName}\" was removed from your list."
                }
            }
        } catch (e: PSQLException) {
            e.printStackTrace()
            "Error. An unknown database error has occurred."
        } catch (e: Exception) {
            e.printStackTrace()
            "Error. Please recheck the correctness of provided data."
        }
    }),
    STATS("/stats", {
        try {
            val allSeries = seriesTable[it.chatId]
            """
                |Series overall: ${allSeries.count()}
                |Series in plans: ${allSeries.count { table -> table.seriesStatus eq SeriesStatus.PLANNING }}
                |Series in progress: ${allSeries.count { table -> table.seriesStatus eq SeriesStatus.WATCHING }}
                |Series finished: ${allSeries.count { table -> table.seriesStatus eq SeriesStatus.FINISHED }}
                |Series dropped: ${allSeries.count { table -> table.seriesStatus eq SeriesStatus.DROPPED }}
                |
                |${
                allSeries
                    .sortedBy { table -> table.seriesStatus }
                    .joinToString(separator = "\n|") { series ->
                        val status = if (series.status == SeriesStatus.DROPPED) "dropped" else "currently"
                        val main = "${series.name}: $status at ep. ${series.currentEpisode}"
                        val total = series.totalEpisodes
                        val percentage = if (total != null)
                            " out of $total (${series.currentEpisode * 100 / total}%)"
                        else
                            ""
                        main + percentage
                    }
            }
            """.trimMargin()
        } catch (e: PSQLException) {
            e.printStackTrace()
            "Error. An unknown database error has occurred."
        } catch (e: Exception) {
            e.printStackTrace()
            "Error. Please recheck the correctness of provided data."
        }
    }),
    SHOW("/show", {
        try {
            val requestText = it.text
            if (requestText.trim() == "/show") {
                showHelpText()
            } else {
                val nameAndArguments = parseNameAndArguments(requestText.removePrefix("/show"))
                val series = seriesTable[it.chatId, nameAndArguments.seriesName]
                if (series == null) {
                    "There is no series called \"${nameAndArguments.seriesName}\" in your list."
                } else {
                    """
                        ${series.name} statistics:
                        status: ${series.status}
                        current episode: ${series.currentEpisode}
                        total episode: ${series.totalEpisodes ?: "unknown"}
                        progress: ${series.totalEpisodes?.let { total -> series.currentEpisode * 100 / total } ?: "unknown "}%
                    """.trimIndent()
                }
            }
        } catch (e: PSQLException) {
            e.printStackTrace()
            "Error. An unknown database error has occurred."
        } catch (e: Exception) {
            e.printStackTrace()
            "Error. Please recheck the correctness of provided data."
        }
    }),
    INC("/inc", {
        try {
            val requestText = it.text
            if (requestText.trim() == "/inc") {
                incHelpText()
            } else {
                val nameAndArguments = parseNameAndArguments(requestText.removePrefix("/inc"))
                val series = seriesTable[it.chatId, nameAndArguments.seriesName]
                if (series == null) {
                    "There is no series called \"${nameAndArguments.seriesName}\" in your list."
                } else {
                    val num = nameAndArguments.arguments["num"]?.toInt() ?: 1
                    series.currentEpisode += num
                    series.status = deriveSeriesStatus(series.currentEpisode, series.totalEpisodes)
                    series.flushChanges()
                    "Series \"${series.name}\" was successfully updated."
                }
            }
        } catch (e: PSQLException) {
            e.printStackTrace()
            "Error. An unknown database error has occurred."
        } catch (e: Exception) {
            e.printStackTrace()
            "Error. Please recheck the correctness of provided data."
        }
    }),
    HELP("/help", {
        helpText()
    }),
    START("/start", {
        """
            |Hello! It's your fellow series secretary! I'll help you track your series progress. Please, read help text:
            |${helpText()}
        """.trimMargin()
    })
}
