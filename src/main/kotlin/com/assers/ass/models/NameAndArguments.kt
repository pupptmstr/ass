package com.assers.ass.models

data class NameAndArguments (
    val seriesName: String,
    val arguments: Map<String, String>
)
