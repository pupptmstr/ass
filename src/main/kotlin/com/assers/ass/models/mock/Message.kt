package com.assers.ass.models.mock

import org.telegram.telegrambots.meta.api.objects.Update

data class Message(
    val text: String,
    val chatId: Long
) {
    constructor(update: Update) : this(update.message.text, update.message.chatId)
}
