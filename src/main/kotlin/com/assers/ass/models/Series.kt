package com.assers.ass.models

import org.ktorm.entity.Entity

interface Series : Entity<Series> {
    var userId: Long
    var name: String
    var status: SeriesStatus
    var currentEpisode: Int
    var totalEpisodes: Int?

    companion object : Entity.Factory<Series>()
}
