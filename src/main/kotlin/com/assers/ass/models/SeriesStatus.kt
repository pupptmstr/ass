package com.assers.ass.models

enum class SeriesStatus {
    PLANNING,
    WATCHING,
    FINISHED,
    DROPPED;

    companion object {
        val ddlExpression = """
            CREATE TYPE series_status AS ENUM(${values().map { it.name }.joinToString { "'$it'" }});
        """.trimIndent()
    }
}
