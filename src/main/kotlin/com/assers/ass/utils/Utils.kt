package com.assers.ass.utils

fun helpText(): String = """
    |Bot commands:
    |/start — start working with your secretary (shows this help text)
    |/add <series' name> --status <status> --current <episode's number> --total <number of episodes> — add a new series to your list
    |/update <series' name> --status <status> --current <episode's number> --total <number of episodes> — update the series from your list
    |/remove <series' name> — remove the series from your list
    |/stats — calculate some statistics and make a summary based on your list
    |/show <series' name> — show where you left a particular series from your list
    |/inc <series' name> --num <number of episodes> — handily track your watching progress of the series
""".trimMargin()

fun addHelpText() : String = """
    |To add a new series to your list, send a message formatted as:
    |/add <series' name> --status <status> --current <episode's number> --total <number of episodes>
    |
    |Statuses can be:
    |planning
    |watching
    |finished
    |dropped
    |
    |The status may be inferred from other data: if current episode's number is 0 then the status is "planning", if it's bigger than or equal to the total number of episodes then the status is "finished", otherwise it is "watching".
    |The total number of the episodes may be omitted.
    |
    |Example:
    |/add My Hero Academy 3 --status watching --current 15 --total 25
""".trimMargin()

fun showHelpText(): String = """
    |To show where you left a particular series from your list, send a message formatted as:
    |/show <series' name>
""".trimMargin()

fun updateHelpText(): String = """
    |To update a series from your list, send a message formatted as:
    |/update <series' name> --status <status> --current <episode's number> --total <number of episodes>
    |
    |Statuses can be:
    |planning
    |watching
    |finished
    |dropped
    |
    |Any omitted arguments will not be updated.
    |To update the total number of the series, send anything that's not a number (e.g. "null").
    |
    |Example:
    |/update My Hero Academy 3 --status watching --current 15 --total 25
""".trimMargin()

fun removeHelpText() : String = """
    |To update a series from your list, send a message formatted as:
    |/remove <series' name>
    |
    |Example:
    |/remove My Hero Academy 3
""".trimMargin()

fun incHelpText() : String = """
    |To track your series' progress, send a message formatted as:
    |/inc <series' name> --num <number of episodes>
    |
    |The episode counter for the series in question will be increased by a provided value (1 is the default).
    |
    |Example:
    |/inc My Hero Academy 3 --num 6
""".trimMargin()
