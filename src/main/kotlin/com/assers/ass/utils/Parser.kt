package com.assers.ass.utils

import com.assers.ass.models.NameAndArguments

fun parseNameAndArguments(text: String): NameAndArguments {
    var textCopy = text
    val res = mutableMapOf<String, String>()
    val keyAndArgumentRegexp = """(--[a-z]+ [a-zA-Z0-9а-яА-я]+)""".toRegex()
    val matchResults = keyAndArgumentRegexp.findAll(text)
    for (matchResult in matchResults) {
        val keyAndValue = matchResult.value.split(" ")
        val key = keyAndValue[0].replace("--", "")
        val value = keyAndValue[1]
        res[key] = value
        textCopy = textCopy.replace(key, "").replace(value, "")
    }
    val name = textCopy.replace("--", "").trim().ifEmpty {
        throw IllegalArgumentException("The series' name wasn't provided")
    }
    return NameAndArguments(name, res)
}
