package com.assers.ass

import com.assers.ass.models.Commands
import com.assers.ass.models.Commands.HELP
import com.assers.ass.models.mock.Message
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.exceptions.TelegramApiException

class Bot : TelegramLongPollingBot() {
    private val logger: Logger = LoggerFactory.getLogger(Bot::class.java)

    private val botKey: String = System.getenv("ASS_TG_BOT_TOKEN_KEY").takeUnless { it.isNullOrEmpty() }
        ?: throw IllegalStateException("No Telegram bot key was specified in the execution environment")
    private val botName: String = System.getenv("ASS_TG_BOT_NAME").takeUnless { it.isNullOrEmpty() }
        ?: throw IllegalStateException("No Telegram bot name was specified in the execution environment")

    init {
        logger.info("botKey and botName properties were successfully read from environment")
    }

    override fun getBotToken(): String = botKey

    override fun getBotUsername() = botName

    override fun onUpdateReceived(update: Update?) {
        if (update != null) {
            if (update.hasMessage()) {
                if (update.message.hasText()) {
                    val text = update.message.text
                    val command = Commands.values().find { command -> text.startsWith(command.text) } ?: HELP
                    respond(command.action(Message(update)), update)
                } else {
                    // TODO process empty message
                    respondNotImplemented(update)
                }
            }
        }
    }

    private fun respond(text: String, update: Update) {
        val message = SendMessage()
        message.chatId = update.message.chatId.toString()
        message.text = text
        try {
            execute(message)
            logger.info("Bot has sent a response to client '${getUserName(update)}'.")
        } catch (e: TelegramApiException) {
            logger.error(e.stackTraceToString())
        }
    }

    private fun getUserName(update: Update): String = update.message.from.userName

    private fun respondNotImplemented(update: Update) = respond("This feature is not implemented yet", update)
}
