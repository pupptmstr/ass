package com.assers.ass.database

import com.assers.ass.models.Series
import com.assers.ass.models.SeriesStatus
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.ktorm.database.Database
import org.ktorm.dsl.and
import org.ktorm.dsl.eq
import org.ktorm.entity.EntitySequence
import org.ktorm.entity.add
import org.ktorm.entity.filter
import org.ktorm.entity.find
import org.ktorm.entity.sequenceOf
import org.slf4j.Logger
import org.slf4j.LoggerFactory

val seriesTable get() = database.sequenceOf(SeriesTable)

// a single entity described by userId and seriesName if it exists
operator fun EntitySequence<Series, SeriesTable>.get(userId: Long, seriesName: String): Series? =
    find { table -> (table.userId eq userId) and (table.seriesName eq seriesName) }

// all entities with provided userId
operator fun EntitySequence<Series, SeriesTable>.get(userId: Long): EntitySequence<Series, SeriesTable> =
    filter { table -> table.userId eq userId }

fun EntitySequence<Series, SeriesTable>.add(
    userId: Long,
    name: String,
    status: SeriesStatus? = null,
    currentEpisode: Int = 0,
    totalEpisodes: Int? = null
) {
    val newSeries = Series {
        this.userId = userId
        this.name = name
        this.currentEpisode = currentEpisode
        this.totalEpisodes = totalEpisodes
        this.status = status ?: deriveSeriesStatus(currentEpisode, totalEpisodes)
    }
    add(newSeries)
}

fun deriveSeriesStatus(currentEpisode: Int, totalEpisodes: Int?): SeriesStatus = when {
    currentEpisode == 0 -> SeriesStatus.PLANNING
    totalEpisodes != null && currentEpisode >= totalEpisodes -> SeriesStatus.FINISHED
    else -> SeriesStatus.WATCHING
}

data class DatabaseInfo(
    val jdbcUrl: String,
    val username: String,
    val password: String
)

fun prepareDatabase(info: DatabaseInfo) {
    val config = HikariConfig().apply {
        jdbcUrl = info.jdbcUrl
        username = info.username
        password = info.password
        driverClassName = "org.postgresql.Driver"
    }
    database = Database.connect(HikariDataSource(config))
    logger.info("Connection to the bot's Postgres database was successful.")

    database.useConnection { connection ->
        connection.prepareStatement("""
            DO ${'$'}${'$'}
            BEGIN
                IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'series_status') THEN
                    ${SeriesStatus.ddlExpression}
                END IF;
            END
            ${'$'}${'$'};
        """.trimIndent()).execute()
        connection.prepareStatement(SeriesTable.ddlExpression).execute()
    }
    logger.info("The bot's Postgres database was successfully initialized.")
}

private lateinit var database: Database

private val logger: Logger = LoggerFactory.getLogger("com.assers.ass.database.UtilKt")
