package com.assers.ass.database

import com.assers.ass.models.Series
import com.assers.ass.models.SeriesStatus
import org.ktorm.schema.Table
import org.ktorm.schema.enum
import org.ktorm.schema.int
import org.ktorm.schema.long
import org.ktorm.schema.text

object SeriesTable : Table<Series>("series") {
    val userId = long("user_id").primaryKey().bindTo { it.userId }
    val seriesName = text("series_name").primaryKey().bindTo { it.name }
    val seriesStatus = enum<SeriesStatus>("series_status").bindTo { it.status }
    val currentEpisode = int("current_episode").bindTo { it.currentEpisode }
    val totalEpisodes = int("total_episodes").bindTo { it.totalEpisodes }

    val ddlExpression = """
        CREATE TABLE IF NOT EXISTS $tableName(
            ${userId.name} ${userId.sqlType.typeName} NOT NULL,
            ${seriesName.name} ${seriesName.sqlType.typeName} NOT NULL,
            ${seriesStatus.name} series_status NOT NULL,
            ${currentEpisode.name} ${currentEpisode.sqlType.typeName} NOT NULL,
            ${totalEpisodes.name} ${totalEpisodes.sqlType.typeName},
            PRIMARY KEY (${userId.name}, ${seriesName.name})
        );
    """.trimIndent()
}
