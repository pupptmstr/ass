package com.assers.ass

import com.assers.ass.database.DatabaseInfo
import com.assers.ass.database.prepareDatabase
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession

private val logger: Logger = LoggerFactory.getLogger("com.assers.ass.MainKt")

fun main() {
    try {
        val dbName = System.getenv("POSTGRES_DB").takeUnless { it.isNullOrEmpty() }
            ?: throw IllegalStateException("No bot database's name was specified in the execution environment")
        val dbUsername = System.getenv("POSTGRES_USER").takeUnless { it.isNullOrEmpty() }
            ?: throw IllegalStateException("No bot database user's name was specified in the execution environment")
        val dbPassword = System.getenv("POSTGRES_PASSWORD").takeUnless { it.isNullOrEmpty() }
            ?: throw IllegalStateException("No bot database user's password was specified in the execution environment")
        prepareDatabase(DatabaseInfo("jdbc:postgresql://database:5432/$dbName", dbUsername, dbPassword))
        logger.info("All database preparation tasks were successfully cleared.")

        val telegramBotsApi = TelegramBotsApi(DefaultBotSession::class.java)
        telegramBotsApi.registerBot(Bot())
        logger.info("Bot successfully initialized, registered, and started working.")
    } catch (e: Exception) {
        // TODO might not necessarily be troubles with environment, should be traced and checked someday
        logger.error(e.stackTraceToString())
        logger.error("Problems were detected! Check if environment was set up correctly.")
    }
}
