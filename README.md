# A Series Secretary

[![pipeline status](https://gitlab.com/pupptmstr/ass/badges/main/pipeline.svg)](https://gitlab.com/pupptmstr/ass/-/commits/main)

A Telegram bot that helps you to track your series progress.

## Setup

0. Make sure you have Docker and Docker Compose installed on your machine.
1. Setup a Telegram bot and get its credentials per Telegram-regulated procedures.
2. Make a local copy of this repository.
3. Fill out files in the `environment` folders with bot credentials acquired at step 1
   and database credentials of your own choosing.
5. Run `sudo docker-compose up`.

After this procedure the bot should be responsive to your Telegram messages.

## Functionality

This bot is able to:

- add a new series to your list via `/add` command;
- update the series from your list via `/update` command;
- remove the series from your list via `/remove` command;
- calculate some statistics and make a summary based on your list via `/stats` command;
- show where you left a particular series from your list via `/show` command;
- handily track your watching progress of the series via `/inc` command.

You can learn more about all commands via the `/help` command or about a particular
command (sans `/stats`) via running that command without any arguments.

Following info is tracked about each series you enter into bot's database:

- its name, obviously;
- the status of you watching it (you may be planning to watch the series, be watching it at the moment,
  have already finished it, or have dropped it altogether at some point);
- the episode you are currently at;
- total number of episodes (this attribute may be left out).
