# build stage
FROM gradle:7.1-jdk11 AS build

WORKDIR /project

COPY src ./src
COPY *.gradle ./
COPY gradle.properties ./

RUN gradle shadowJar

# execution stage
FROM openjdk:11

WORKDIR /app

COPY --from=build /project/build/libs/ass-0.0.0-all.jar ./application.jar

CMD ["java", "-jar", "application.jar"]
